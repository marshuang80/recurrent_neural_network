50 epochs
50 hidden units
sequence size of 10

Plotted average cross entropy loss over epochs. For all the training loss graphs,
the total loss was added up each epoch and averaged over the number of windows used
Note that we used hamlet to train, so while the generated text may seem far from 
an english word, it may be a word used in the training or very similar.

-----
epoch:  0 
-----
   in,
We buid a tive, sies in may;
An heat;
Then thof huthine stoac.

Kant
 ouls wimas:--Congit ag th

-----
epoch:  10 
-----
  iul: most us this he dome coll
So I
Your fic thy bulll, dounder do they hiat wonksbievental.

Port
Fo

-----
epoch:  20 
-----
  e sir defork.
To willind!

Laer.
Now indiond with most of [1outher gat yety thene, ear.--
Thevey spo

-----
epoch:  30 
-----
  fants me's a the know med.]

[Exit.]

King.
And welve!--
Hamlers?

Ham:
Dials what dod cone's our no

-----
epoch:  40 
-----
  as will de, hip eebes, wish that in prayed your her fall it revy s~it
dooly sirpoch all
To suitsest 

-----
epoch:  49 
-----
   my it his wery of it,
That, surs my lord
That you good amast fier in and light: but I heav''s that.
