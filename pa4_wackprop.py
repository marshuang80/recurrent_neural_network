
# coding: utf-8

# In[3]:

import copy
import numpy as np
import struct, math
from array import array
import matplotlib.pyplot as plt
import numpy.matlib as mat
import string
import random
#get_ipython().magic('matplotlib inline')


'''
def load(file, N):
    characters = []
    sentences = []
    prevchar = ''
    i = 0
    with open(file) as file:
        for word in file:  
            for ch in word: 
                if ord(ch) >= 0 and ord(ch) <= 255:
                    characters.append(np.zeros(256))
                    characters[-1][ord(ch)] = 1
                if prevchar == '.' or prevchar == '!' or prevchar == '?':
                    sentences.append(np.array(characters))
                    characters = []
                    i += 1
                    if i == N:
                        return sentences
                prevchar = ch
                
    return sentences
'''

def load(filename):
    '''
    characters = []
    with open(filename) as f:
        data = f.read()
    chars = list(data)
    #for i in range (100):
    print((chars[0]))
    vectors = np.zeros([len(chars), 256])
    i = 0
    for v, c in zip(vectors, chars):
        i+=1
        if ord(c)>=0 and ord(c)<=255:
            vectors[ord(c)-1][i] = 1
    return vectors
    '''
    characters = []
    with open(filename,'rt',encoding = 'utf-8') as file:
        for word in file:  
            for ch in word: 
                if ord(ch) >= 0 and ord(ch) <= 255:
                    characters.append(np.zeros(256))
                    characters[-1][ord(ch)] = 1
    char = np.array(characters)
    return char



# In[4]:

def div0( a, b ):
    with np.errstate(divide='ignore', invalid='ignore'):
        c = np.true_divide( a, b )
        c[ ~ np.isfinite( c )] = 0  # -inf inf NaN
    return c

def net(x,w):
    res = np.dot(x,w)
    return res
def one_hot(y):
    labels = np.zeros((50000,10))
    for i in range(len(train_labels)):
        labels[i][int(train_labels[i])] = 1
    return  labels

def  getPred(p):
    return [np.argmax(row) for row in p]

def  getPred_sto(p):
    return np.argmax(p)

def gradient(x,y):
    return np.dot(x.T, y)

def get_error(pred,target):
    error = 0
    for p,t in zip(pred,target):
        if p != t:
            error +=1
    return float(error)/len(target)

def sigmoid_func(x):   
    return 1 / (1 + math.exp(-x))

def sigmoid(z, der = False):
    #z -= np.max(z)
    if der == True: 
        return z * (1 - z)
    f = np.vectorize(sigmoid_func)
    return f(z)

def tanh_func(x):
    return (np.exp(x).T - np.exp(-x).T)/(np.exp(x).T + np.exp(-x).T)
def tanh(z,der = False):
    #z -= np.max(z)
    if der == True: 
        return 1 - np.power(z,2)
    f = np.vectorize(tanh_func)
    return f(z)
def ReLU(z,der = False):
    #z -= np.max(z)
    if der == True: 
        z[z > 0] = 1
        z[z<=0] = 0
        return z
    return z.clip(0)


def softmax(y, temp = 1):
    return np.exp(y/temp) / np.sum(np.exp(y/temp))

def cross_entropy(t,w,x,label,WHO):
    E = 0
    net_out = net(x,w)
    if label == "IH":
        hid = sigmoid(net_out)
        hid = np.insert(hid, 0, np.ones(50000),1)
        y = softmax(net(hid,WHO))
    else:
        y = softmax(net_out) 
    E += (t*np.log(y)).sum()
    return E

def gradient_check(t,w,x,label,WHO):
    row,col = w.shape
    gradApprox = np.zeros((row,col))
    e = 1e-2
    for i in range(row):
        for j in range(col):
            wPlus = w.copy()
            wPlus[i][j] +=  e
            wMinus = w.copy()
            wMinus[i][j] -= e
            gradApprox[i][j] = (cross_entropy(t,wPlus,x,label,WHO)-cross_entropy(t,wMinus,x,label,WHO))/(2*e)
    return gradApprox

def update_weights(W,alpha,delta,layer, delta_weight = None,lamb = None, gamma = None):
    grad = gradient(layer, delta)
    if grad.shape != W.shape:
        grad = grad[:,1:]
    if lamb != None : 
        reg = lamb  * W.sum()
        return W + alpha*(grad - lamb)
    if gamma != None: 
        return W + alpha*grad + gamma *(delta_weight)

    return W + alpha *grad

def diff_weights(W,old_weight,t):
    if t == 0:
        return np.zeros((W.shape)), W
    return (W-old_weight),W

def print_grad(delta,gradApprox):
    print("average difference: ",((delta)-(gradApprox)).mean()/10)
    for row1,row2 in zip(delta,gradApprox):
        if row1[0] != 0.0:
            print("backprop: ",(row1[:4]))
            print("gradient: ",(row2[:4]))
            print("------------------------------------------------------------------------------")


# In[ ]:

# RNN code
vocab_size = 256
hidden_size = 50
seq_size = 10

def initialize_weights(hidden_size, vocab_size):
    # Weights
    Wxh = np.random.normal(0, 1/np.sqrt(vocab_size), [hidden_size, vocab_size])
    Whh = np.random.normal(0, 1/np.sqrt(hidden_size), [hidden_size, hidden_size])
    Why = np.random.normal(0, 1/np.sqrt(hidden_size), [vocab_size, hidden_size])
    # Biases
    bh = np.random.normal(0, 1/np.sqrt(hidden_size), [hidden_size, 1])
    by = np.random.normal(0, 1/np.sqrt(hidden_size), [vocab_size, 1])
    return Wxh, Whh, Why, bh, by

Wxh, Whh, Why, bh, by = initialize_weights(hidden_size, vocab_size)


def forward(seq, hprev, temperature=1):
    acc = 0
    total = 0 
    error = 0
    h, y, p = {}, {}, {}
    h[-1] = np.copy(hprev)
    for t in range(len(seq)-1):
        h[t] = tanh((np.dot(Wxh, seq[t])) + np.dot(Whh, h[t-1]) + bh)
        y[t] = np.dot(Why, h[t]) + by
        p[t] = softmax(y[t], temperature)
        #print(np.where(p[t]==np.max(p[t])))
        #print("***")
        # we use t+1 because we're trying to predict the next
        # character in the sequence
        error += -np.log(p[t][np.argmax(seq[t+1])])
        total += 1
        #if t == 40:
            #print (np.where(seq[t+1]==1)[0], np.where(p[t] == np.max(p[t]))[0])
        if np.argmax(seq[t+1]) == np.argmax(p[t]):
            #print("here")
            acc += 1
        #print(np.where(p[t]==1))
        #print(np.where(seq[t+2]==1))
        #print("***")
            #print(acc)
    return h, y, p, error, acc, total, hprev

def backprop(seq, h, o, p, error):
    dWhy = np.zeros_like(Why)
    dWhh = np.zeros_like(Whh)
    dWxh = np.zeros_like(Wxh)
    dby = np.zeros_like(by)
    dbh = np.zeros_like(bh)
    dhnext = np.zeros_like(bh)
    for t in range(len(seq)-2, -1, -1):
        dy = p[t].copy()
        #print p[t].shape
        dy[np.where(seq[t+1]==1)] -= 1
        dby += dy
        dWhy += np.dot(dy, h[t].T)
        dh = np.dot(Why.T, dy) + dhnext
        
        #dhnet = tanh(h[t], True) * dh
        dhnet = (1 - h[t]**2) * dh
        dbh += dhnet
        dWxh += np.dot(dhnet, seq[t].T.reshape(1,vocab_size))
        dWhh += np.dot(dhnet, h[t-1].T)
        dhnext = np.dot(Whh.T, dhnet)
    return dWhy, dWhh, dWxh, dby, dbh
    
def sample(p):
    rand = random.uniform(0,1)
    probabilities = dict(enumerate(p))
    sort_list = sorted(probabilities.items(), key=lambda x: x[1])
    key = []
    val = []
    for i in range(len(sort_list)):
        key.append(sort_list[i][0])
        val.append(sort_list[i][1])
    for i in range(len(val)):
        if i != 0:
            val[i] += val[i-1]
    letter = 0
    for i in range(len(val)):
        if val[i] > rand: 
            letter = key[i]
            break
        if i == len(val)-1:
            letter = key[i]
    return letter


def generate(Why, Whh, Wxh, by, bh):
    # Set the temperature to change the variance
    temperature = 1.0
    hprev = np.zeros([hidden_size, 1])

    # Generate random uppercase character and use as index for one hot encoding
    x = np.zeros([256, seq_size])
    x[ord(random.choice(string.ascii_lowercase).upper())][-1] = 1
    
    # Generated text length of 100 characters
    textLength = 100
    
    # Create empty output string to eventually be populated with characters
    output = ''
    
    for i in range(0, textLength):
        output += str(chr(np.argmax(x[:,-1])))
        
        # Forward propagate one step with a temperature
        h, y, p, error, acc,total, hprev = forward(x.T, hprev, temperature)
        x = np.delete(x, 0, 1)
        x = np.append(x, p[seq_size-2], 1)
        xInd = sample(x[:, -1])
        x[:, -1] = np.zeros(256)
        x[xInd, -1] = 1
    print(output)
        
def train(trainInputs, testInputs):
    global Why
    global Whh
    global Wxh
    global by
    global bh
    hprev = np.zeros([hidden_size, 1])

    accuracy = []
    test_accuracy = []
    epochs = 10
    alpha = 0.001
    print ("Total iterations: {}".format((len(trainInputs) - seq_size)))
    for e in range(0, epochs):
        generate(Why, Whh, Wxh, by, bh)
        acc = 0
        step = 0 
        total = 0 
        test_total = 0 
        for i in range(0, len(trainInputs) - seq_size, int(seq_size)):
            step += 1
            if step % 1000==0:
                print (step)
            #if e >= 70 and i % 1000:
            #    print (i)
            # We feed in the inputs{t} and use inputs{t+1} as the target
            # which is why we need to use i+seq_size+1
            seq = trainInputs[i:i+seq_size+1]
            #print(np.argmax(seq[1]))
            h, o, p, error, a, t, hprev = forward(seq, hprev)
            total += t
            acc += a
            dWhy, dWhh, dWxh, dby, dbh = backprop(seq, h, o, p, error)

            mWxh = np.zeros_like(dWxh)
            mWhh = np.zeros_like(dWhh)
            mWhy = np.zeros_like(dWhy)
            mbh = np.zeros_like(dbh)
            mby = np.zeros_like(dby)
            
            mWxh += dWxh * dWxh
            mWhh += dWhh * dWhh
            mWhy += dWhy * dWhy
            mbh += dbh * dbh
            mby += dby * dby
            
            Wxh -= (alpha * dWxh) / np.sqrt(mWxh + 1e-8)
            Whh -= (alpha * dWhh) / np.sqrt(mWhh + 1e-8)
            Why -= (alpha * dWhy) / np.sqrt(mWhy + 1e-8)
            by -= (alpha * dby) / np.sqrt(mby + 1e-8)
            bh -= (alpha * dbh) / np.sqrt(mbh + 1e-8)
            
            '''
            Why_old = Why.copy()
            Whh_old = Whh.copy()
            Wxh_old = Wxh.copy()
            by_old = by.copy()
            bh_old = bh.copy()
            
            Why -= alpha * dWhy
            Whh -= alpha * dWhh
            Wxh -= alpha * dWxh
            by -= alpha * dby
            bh -= alpha * dbh
            '''
        # TODO Calculate this differently
        accuracy.append(acc/(total))
        #accuracy.append(acc)
        print("train: ", accuracy)
        
        h, o, p, error, ta, test_total, hprev = forward(testInputs, hprev)
        # TODO Calculate this differently
        test_accuracy.append(ta/test_total)
        #test_accuracy.append(ta)
        print("test: ", test_accuracy)
    
    plt.plot(accuracy)
    plt.plot(test_accuracy)
    plt.title('Training/Testing Over Epochs')
    plt.ylabel('accuracy')
    plt.xlabel('epoch')
    plt.legend(['train', 'test'], loc='upper left')
    plt.show()
        



# In[ ]:

trainInputs = load('data/hamlet.txt')
testInputs = load('data/hamletTest.txt')
train(trainInputs, testInputs)
generate(Why, Whh, Wxh, by, bh)


# In[ ]:



