import numpy as np
import random
import string
import matplotlib.pyplot as plt

epochs = 50
alpha = .1
beta = .00000001
num_hidden1 = 50
num_hidden2 = 50
batch_size = 10
chars = {}
charsRev = {}

def randInit():
    Wh1h2 = np.random.normal(0, 1/np.sqrt(num_hidden1), [num_hidden2, num_hidden1])
    Whh1 = np.random.normal(0, 1/np.sqrt(num_hidden1), [num_hidden1, num_hidden1])
    Whh2 = np.random.normal(0, 1/np.sqrt(num_hidden2), [num_hidden2, num_hidden2])
    Wxh = np.random.normal(0, 1/np.sqrt(256), [num_hidden1, 256])
    Why = np.random.normal(0, 1/np.sqrt(num_hidden1), [256, num_hidden1])
    bh1 = np.random.normal(0, 1/np.sqrt(num_hidden1), [num_hidden1, 1])
    bh2 = np.random.normal(0, 1/np.sqrt(num_hidden1), [num_hidden2, 1])
    by = np.random.normal(0, 1/np.sqrt(num_hidden1), [256, 1])
    return Wh1h2, Whh1, Whh2, Wxh, Why, bh1, bh2, by

def initCorpus():
    corpus = open('data/hamlet.txt', 'r').read()
    for l,char in enumerate(list(set(corpus))):
        chars[l] = char
    for l,char in enumerate(list(set(corpus))):
        charsRev[char] = l
    return corpus

Wh1h2, Whh1, Whh2, Wxh, Why, bh1, bh2, by = randInit()
adaWh1h2 = np.zeros_like(Wh1h2)
adaWhh1 = np.zeros_like(Whh1)
adaWhh2 = np.zeros_like(Whh2)
adaWxh = np.zeros_like(Wxh)
adaWhy = np.zeros_like(Why)
adabh2 = np.zeros_like(bh2)
adabh1 = np.zeros_like(bh1)
adaby = np.zeros_like(by)
trainLoss = []

# compute gradients for weight updates
def computeGradients(inputChars, predTargets, lastHidState, temperature=1):

    train_loss = 0

    # create empty dicts for forward/back prop
    inp = {}
    h1 = {}
    h2 = {}
    y = {}
    p = {}
    h1[-1] = np.copy(lastHid1State)
    h2[-1] = np.copy(lastHid2State)
    acc = 0
    for i in range(len(inputChars)):
        inp[i] = np.zeros((256,1))
        inp[i][inputChars[i]] = 1
        h1[i] = np.tanh(np.dot(Wxh, inp[i]) + np.dot(Whh1, h1[i-1]) + bh1)
        h2[i] = np.tanh((np.dot(Wh1h2, h1[i]) + np.dot(Whh2, h2[i-1]) + bh2))
        y[i] = np.dot(Why, h2[i]) + by
        p[i] = np.exp(y[i]) / np.sum(np.exp(y[i]))
        train_loss += -np.log(p[i][predTargets[i],0])
        if predTargets[i] == np.where(p[i] == np.max(p[i]))[0]:
            acc += 1

    gradWhy = np.zeros_like(Why)
    gradWh1h2 = np.zeros_like(Wh1h2)
    gradWhh1 = np.zeros_like(Whh1)
    gradWhh2 = np.zeros_like(Whh2)
    gradWxh = np.zeros_like(Wxh)
    gradby = np.zeros_like(by)
    gradbh1 = np.zeros_like(bh1)
    gradbh2 = np.zeros_like(bh2)
    gradh1next = np.zeros_like(bh1)
    gradh2next = np.zeros_like(bh2)
    total = 0
    for t in reversed(range(len(inputChars))):
        total +=1
        grady = np.copy(p[t])
        grady[predTargets[t]] -= 1
        ##### Look into this
        gradby += grady
        gradWhy += np.dot(grady, h2[t].T)
        gradh2 = np.dot(Why.T, grady) + gradh2next

        gradh2net = (1 - h2[t]**2) * gradh2
        gradbh2 += gradh2net
        gradWh1h2 += np.dot(gradh2net, h1[t].T.reshape(1,num_hidden1))
        gradWhh2 += np.dot(gradh2net, h2[t-1].T)
        gradh2next = np.dot(Whh2.T, gradh2net)
        gradh1 = np.dot(Wh1h2.T, gradh2) + gradh1next
        gradh1net = (1 - h1[t]**2) * gradh1
        gradbh1 += gradh1net
        gradWhh1 += np.dot(gradh1net, h1[t-1].T)
        gradWxh += np.dot(gradh1net, inp[t].T)
        gradh1next = np.dot(Whh1.T, gradh1net)

    return acc, total, gradWhy, gradWh1h2, gradWhh1, gradWhh2, gradWxh, gradby, gradbh1, gradbh2, h1[len(inputChars)-1], h2[len(inputChars)-1], train_loss

# output a sequence using trained network
def generateSequence(h1, h2, temp=1):

    startChar = ord(random.choice(string.ascii_lowercase).upper())
    x = np.zeros((256, 1))
    x[startChar] = 1
    sequence = []
    for t in range(0,100):
        h1 = np.tanh(np.dot(Wxh, x) + np.dot(Whh1, h1) + bh1)
        h2 = np.tanh(np.dot(Wh1h2, h1) + np.dot(Whh2, h2) + bh2)
        y = np.dot(Why, h2) + by
        p = (np.exp(y/temp)) / (np.sum(np.exp(y/temp)))

        #make sure generated character is actually from our training text
        nextChar = len(chars.keys()) + 1
        while len(chars.keys()) < nextChar:
            nextChar = np.random.choice(range(256), p=p.ravel())

        x = np.zeros((256, 1))
        x[nextChar] = 1
        sequence.append(nextChar)

    return sequence

for e in range(0,epochs):
    corpus = initCorpus()
    lastHid1State = np.zeros((num_hidden1,1))
    lastHid2State = np.zeros((num_hidden1,1))
    training_loss = 0

    for ptr in range(0, len(corpus)-batch_size, batch_size):

        inp, targ = [], []
        for ch in corpus[ptr:ptr+batch_size]:
            inp.append(charsRev[ch])
        for ch in corpus[ptr+1:ptr+batch_size+1]:
            targ.append(charsRev[ch])

        a, t, gradWhy, gradWh1h2, gradWhh1, gradWhh2, gradWxh, gradby, gradbh1, gradbh2, lastHid1State, lastHid2State, loss = computeGradients(inp, targ, lastHid1State, lastHid2State)
        training_loss += loss

        adaWxh += gradWxh * gradWxh
        adaWh1h2 += gradWh1h2 * gradWh1h2
        adaWhh1 += gradWhh1 * gradWhh1
        adaWhh2 += gradWhh2 * gradWhh2
        adaWhy += gradWhy * gradWhy
        adabh1 += gradbh1 * gradbh1
        adabh2 += gradbh2 * gradbh2
        adaby += gradby * gradby

        Wxh -= (alpha * gradWxh) / np.sqrt(adaWxh + beta)
        Wh1h2 -= (alpha * gradWh1h2) / np.sqrt(adaWh1h2 + beta)
        Whh1 -= (alpha * gradWhh1) / np.sqrt(adaWhh1 + beta)
        Whh2 -= (alpha * gradWhh2) / np.sqrt(adaWhh2 + beta)
        Why -= (alpha * gradWhy) / np.sqrt(adaWhy + beta)
        by -= (alpha * gradby) / np.sqrt(adaby + beta)
        bh1 -= (alpha * gradbh1) / np.sqrt(adabh1 + beta)
        bh2 -= (alpha * gradbh2) / np.sqrt(adabh2 + beta)

    trainLoss.append(training_loss/(len(corpus)/batch_size))

    if e%10 == 0 or e==(epochs-1):
        newSequence = generateSequence(lastHid1State, lastHid2State)
        newSeq = ''.join(chars[num] for num in newSequence)
        print('epoch: ', e, '\n ', newSeq)
        print('loss: ', trainLoss[-1])

#temperature code
'''
print('5 outputs no temp:\n')
newSequence = generateSequence(lastHidState)
newSeq = ''.join(chars[num] for num in newSequence)
print('output1: \n', newSeq, '\n')
newSequence = generateSequence(lastHidState)
newSeq = ''.join(chars[num] for num in newSequence)
print('output2: \n', newSeq, '\n')
newSequence = generateSequence(lastHidState)
newSeq = ''.join(chars[num] for num in newSequence)
print('output3: \n', newSeq, '\n')
newSequence = generateSequence(lastHidState)
newSeq = ''.join(chars[num] for num in newSequence)
print('output4: \n', newSeq, '\n')
newSequence = generateSequence(lastHidState)
newSeq = ''.join(chars[num] for num in newSequence)
print('output5: \n', newSeq, '\n')

# generate new sequences with temperature
print('temperature 0.05:\n')
newSequence = generateSequence(lastHidState, 0.05)
newSeq = ''.join(chars[num] for num in newSequence)
print('output1: \n', newSeq, '\n')
newSequence = generateSequence(lastHidState, 0.05)
newSeq = ''.join(chars[num] for num in newSequence)
print('output2: \n', newSeq, '\n')
newSequence = generateSequence(lastHidState, 0.05)
newSeq = ''.join(chars[num] for num in newSequence)
print('output3: \n', newSeq, '\n')
newSequence = generateSequence(lastHidState, 0.05)
newSeq = ''.join(chars[num] for num in newSequence)
print('output4: \n', newSeq, '\n')
newSequence = generateSequence(lastHidState, 0.05)
newSeq = ''.join(chars[num] for num in newSequence)
print('output5: \n', newSeq, '\n')

print('-----')
print('temperature 2:\n')
newSequence = generateSequence(lastHidState, temp=2)
newSeq = ''.join(chars[num] for num in newSequence)
print('output1: \n', newSeq, '\n')
newSequence = generateSequence(lastHidState, temp=2)
newSeq = ''.join(chars[num] for num in newSequence)
print('output2: \n', newSeq, '\n')
newSequence = generateSequence(lastHidState, temp=2)
newSeq = ''.join(chars[num] for num in newSequence)
print('output3: \n', newSeq, '\n')
newSequence = generateSequence(lastHidState, temp=2)
newSeq = ''.join(chars[num] for num in newSequence)
print('output4: \n', newSeq, '\n')
newSequence = generateSequence(lastHidState, temp=2)
newSeq = ''.join(chars[num] for num in newSequence)
print('output5: \n', newSeq, '\n')

print('-----')
print('temperature 3:\n')
newSequence = generateSequence(lastHidState, 3)
newSeq = ''.join(chars[num] for num in newSequence)
print('output1: \n', newSeq, '\n')
newSequence = generateSequence(lastHidState, 3)
newSeq = ''.join(chars[num] for num in newSequence)
print('output2: \n', newSeq, '\n')
newSequence = generateSequence(lastHidState, 3)
newSeq = ''.join(chars[num] for num in newSequence)
print('output3: \n', newSeq, '\n')
newSequence = generateSequence(lastHidState, 3)
newSeq = ''.join(chars[num] for num in newSequence)
print('output4: \n', newSeq, '\n')
newSequence = generateSequence(lastHidState, 3)
newSeq = ''.join(chars[num] for num in newSequence)
print('output5: \n', newSeq, '\n')
'''

plt.plot(trainLoss)
plt.title('Average Training Loss Over Epochs')
plt.ylabel('Loss')
plt.xlabel('Epoch')
plt.legend(['train loss'], loc='upper left')
plt.show()
