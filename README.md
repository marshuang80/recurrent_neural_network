# CSE-190-PA4
To run the original network, type
python pa4.py

and to run with temperature experiments, first uncomment them at the bottom of pa4.py, then
python pa4.py

To run the extra credit network, type
python pa4_ExtraCredit.py

and to run with temperature experiments, first uncomment them at the bottom of pa4_ExtraCredit.py, then
python pa4_ExtraCredit.py
