import numpy as np
import random
import string
import matplotlib.pyplot as plt

epochs = 50
alpha = .1
beta = .00000001
num_hidden = 50
batch_size = 10
chars = {}
charsRev = {}

def randInit():
    Wxh = np.random.normal(0, 1/np.sqrt(256), [num_hidden, 256])
    Whh = np.random.normal(0, 1/np.sqrt(num_hidden), [num_hidden, num_hidden])
    Why = np.random.normal(0, 1/np.sqrt(num_hidden), [256, num_hidden])
    bh = np.random.normal(0, 1/np.sqrt(num_hidden), [num_hidden, 1])
    by = np.random.normal(0, 1/np.sqrt(num_hidden), [256, 1])
    return Wxh, Whh, Why, bh, by

def initCorpus():
    corpus = open('data/hamlet.txt', 'r').read()
    for l,char in enumerate(list(set(corpus))):
        chars[l] = char
    for l,char in enumerate(list(set(corpus))):
        charsRev[char] = l
    return corpus

Wxh, Whh, Why, bh, by = randInit()
adaWxh = np.zeros_like(Wxh)
adaWhh = np.zeros_like(Whh)
adaWhy = np.zeros_like(Why)
adabh = np.zeros_like(bh)
adaby = np.zeros_like(by)
trainLoss = []

# compute gradients for weight updates
def computeGradients(inputChars, predTargets, lastHidState, temperature=1, printResult=False):

    train_loss = 0

    # create empty dicts for forward/back prop
    inp = {}
    h = {}
    y = {}
    p = {}
    h[-1] = np.copy(lastHidState)
    acc = 0
    for i in range(len(inputChars)):
        inp[i] = np.zeros((256,1))
        inp[i][inputChars[i]] = 1
        h[i] = np.tanh(np.dot(Wxh, inp[i]) + np.dot(Whh, h[i-1]) + bh)
        y[i] = np.dot(Why, h[i]) + by
        p[i] = np.exp(y[i]) / np.sum(np.exp(y[i]))
        train_loss += -np.log(p[i][predTargets[i],0])
        if predTargets[i] == np.where(p[i] == np.max(p[i]))[0]:
            acc += 1

        if printResult:
            print(chars[np.where(p[i] == np.max(p[i]))[0][0]])

    gradWxh = np.zeros_like(Wxh)
    gradWhh = np.zeros_like(Whh)
    gradWhy = np.zeros_like(Why)
    gradbh = np.zeros_like(bh)
    gradby = np.zeros_like(by)
    futuregradh = np.zeros_like(h[0])
    for i in reversed(range(len(inputChars))):
        grady = np.copy(p[i])
        grady[predTargets[i]] -= 1
        gradWhy += np.dot(grady, h[i].T)
        gradby += grady
        gradh = np.dot(Why.T, grady) + futuregradh
        dhraw = (1 - h[i]**2) * gradh
        gradbh += dhraw
        gradWxh += np.dot(dhraw, inp[i].T)
        gradWhh += np.dot(dhraw, h[i-1].T)
        futuregradh = np.dot(Whh.T, dhraw)

    return gradWxh, gradWhh, gradWhy, gradbh, gradby, h[-1], train_loss

# output a sequence using trained network
def generateSequence(h, temp=1):

    startChar = ord(random.choice(string.ascii_lowercase).upper())
    x = np.zeros((256, 1))
    x[startChar] = 1
    sequence = []
    for t in range(0,100):
        h = np.tanh(np.dot(Wxh, x) + np.dot(Whh, h) + bh)
        y = np.dot(Why, h) + by
        p = (np.exp(y/temp)) / (np.sum(np.exp(y/temp)))

        #make sure generated character is actually from our training text
        nextChar = len(chars.keys()) + 1
        while len(chars.keys()) < nextChar:
            nextChar = np.random.choice(range(256), p=p.ravel())

        x = np.zeros((256, 1))
        x[nextChar] = 1
        sequence.append(nextChar)

    return sequence

for e in range(0,epochs):
    corpus = initCorpus()
    lastHidState = np.zeros((num_hidden,1))
    training_loss = 0

    for ptr in range(0, len(corpus)-batch_size, batch_size):

        inp, targ = [], []
        for ch in corpus[ptr:ptr+batch_size]:
            inp.append(charsRev[ch])
        for ch in corpus[ptr+1:ptr+batch_size+1]:
            targ.append(charsRev[ch])

        gradWxh, gradWhh, gradWhy, gradbh, gradby, lastHidState, loss = computeGradients(inp, targ, lastHidState)
        training_loss += loss

        adaWxh += gradWxh * gradWxh
        adaWhh += gradWhh * gradWhh
        adaWhy += gradWhy * gradWhy
        adabh += gradbh * gradbh
        adaby += gradby * gradby

        Wxh -= (alpha * gradWxh) / np.sqrt(adaWxh + beta)
        Whh -= (alpha * gradWhh) / np.sqrt(adaWhh + beta)
        Why -= (alpha * gradWhy) / np.sqrt(adaWhy + beta)
        by -= (alpha * gradby) / np.sqrt(adaby + beta)
        bh -= (alpha * gradbh) / np.sqrt(adabh + beta)

    if e % 10 == 0:
        sampleText = 'Good Hamlet'
        inpTest, targTest = [], []
        for ch in sampleText[0:batch_size]:
            inpTest.append(charsRev[ch])
        for ch in sampleText[1:batch_size+1]:
            targTest.append(charsRev[ch])

        print(e)
        _, _, _, _, _, _, _ = computeGradients(inpTest, targTest, lastHidState, 1, True)

    trainLoss.append(training_loss/(len(corpus)/batch_size))

    if e%10 == 0 or e==(epochs-1):
        newSequence = generateSequence(lastHidState)
        newSeq = ''.join(chars[num] for num in newSequence)
        print('epoch: ', e, '\n ', newSeq)
        print('loss: ', trainLoss[-1])

#temperature code
'''
print('5 outputs no temp:\n')
newSequence = generateSequence(lastHidState)
newSeq = ''.join(chars[num] for num in newSequence)
print('output1: \n', newSeq, '\n')
newSequence = generateSequence(lastHidState)
newSeq = ''.join(chars[num] for num in newSequence)
print('output2: \n', newSeq, '\n')
newSequence = generateSequence(lastHidState)
newSeq = ''.join(chars[num] for num in newSequence)
print('output3: \n', newSeq, '\n')
newSequence = generateSequence(lastHidState)
newSeq = ''.join(chars[num] for num in newSequence)
print('output4: \n', newSeq, '\n')
newSequence = generateSequence(lastHidState)
newSeq = ''.join(chars[num] for num in newSequence)
print('output5: \n', newSeq, '\n')

# generate new sequences with temperature
print('temperature 0.05:\n')
newSequence = generateSequence(lastHidState, 0.05)
newSeq = ''.join(chars[num] for num in newSequence)
print('output1: \n', newSeq, '\n')
newSequence = generateSequence(lastHidState, 0.05)
newSeq = ''.join(chars[num] for num in newSequence)
print('output2: \n', newSeq, '\n')
newSequence = generateSequence(lastHidState, 0.05)
newSeq = ''.join(chars[num] for num in newSequence)
print('output3: \n', newSeq, '\n')
newSequence = generateSequence(lastHidState, 0.05)
newSeq = ''.join(chars[num] for num in newSequence)
print('output4: \n', newSeq, '\n')
newSequence = generateSequence(lastHidState, 0.05)
newSeq = ''.join(chars[num] for num in newSequence)
print('output5: \n', newSeq, '\n')

print('-----')
print('temperature 2:\n')
newSequence = generateSequence(lastHidState, temp=2)
newSeq = ''.join(chars[num] for num in newSequence)
print('output1: \n', newSeq, '\n')
newSequence = generateSequence(lastHidState, temp=2)
newSeq = ''.join(chars[num] for num in newSequence)
print('output2: \n', newSeq, '\n')
newSequence = generateSequence(lastHidState, temp=2)
newSeq = ''.join(chars[num] for num in newSequence)
print('output3: \n', newSeq, '\n')
newSequence = generateSequence(lastHidState, temp=2)
newSeq = ''.join(chars[num] for num in newSequence)
print('output4: \n', newSeq, '\n')
newSequence = generateSequence(lastHidState, temp=2)
newSeq = ''.join(chars[num] for num in newSequence)
print('output5: \n', newSeq, '\n')

print('-----')
print('temperature 3:\n')
newSequence = generateSequence(lastHidState, 3)
newSeq = ''.join(chars[num] for num in newSequence)
print('output1: \n', newSeq, '\n')
newSequence = generateSequence(lastHidState, 3)
newSeq = ''.join(chars[num] for num in newSequence)
print('output2: \n', newSeq, '\n')
newSequence = generateSequence(lastHidState, 3)
newSeq = ''.join(chars[num] for num in newSequence)
print('output3: \n', newSeq, '\n')
newSequence = generateSequence(lastHidState, 3)
newSeq = ''.join(chars[num] for num in newSequence)
print('output4: \n', newSeq, '\n')
newSequence = generateSequence(lastHidState, 3)
newSeq = ''.join(chars[num] for num in newSequence)
print('output5: \n', newSeq, '\n')
'''

plt.plot(trainLoss)
plt.title('Average Training Loss Over Epochs')
plt.ylabel('Loss')
plt.xlabel('Epoch')
plt.legend(['train loss'], loc='upper left')
plt.show()
